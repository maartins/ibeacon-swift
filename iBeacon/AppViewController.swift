//
//  AppViewController.swift
//  iBeacon
//
//  Created by Martins Saukums on 21/05/16.
//  Copyright © 2016 Martins Saukums. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class AppViewController: UIViewController ,BeaconManagerDelegate, CLLocationManagerDelegate, MagentoDelegate {
    
    var beaconManager: BeaconManager?
    let magento = Magento.magentoSingltone


    @IBOutlet weak var bg: UIImageView!
    @IBOutlet weak var pointLabel: UILabel!
    @IBOutlet weak var customer: UILabel!
    
    override func viewDidLoad() {
        
        // Blur Effect
        var blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        var blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        print(bg.bounds)
        blurEffectView.alpha = 0.4
        bg.addSubview(blurEffectView)
        
        super.viewDidLoad()
        
    
        self.magento.customerReward()
        self.beaconManager = sharedBeaconManager
        self.beaconManager!.delegate = self
        self.magento.delegate = self
        
    }
    
    func discoveredBeacon(major major: NSNumber, minor: NSNumber, proximity: CLProximity) {
        // print("VC major:\(major) minor:\(minor) distance:\(proximity)");
        
    }
    
    func sendData(name: String, points: String) {
        self.pointLabel.text = points
        self.customer.text = "Sveiks \(name) !"
        
    }
    
    func sendCoordinates(xy: String, obj: [CLBeacon]){
        for beacon: CLBeacon in obj {
            //print (beacon)
        }
        if ((beaconManager?.xy) != nil){
            //Label.text = beaconManager!.xy
        }else{
            //Label.text = "No coordinates, only \(obj.count) found"
        }
        
    }
    
    
}