
import CoreLocation
import UIKit
import SwiftyJSON

// General search criteria for beacons that are broadcasting
let BEACON_REGION_NAME = "Estimote Region"
let BEACON_PROXIMITY_UUID = NSUUID(UUIDString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D")

// Beacons are hardcoded into our app so we can easily filter for them in a noisy environment
let BEACON_PURPLE_MAJOR = "43880"
let BEACON_PURPLE_MINOR = "49744"
let BEACON_GREEN_MAJOR = "37513"
let BEACON_GREEN_MINOR = "59322"
let BEACON_BLUE_MAJOR = "19093"
let BEACON_BLUE_MINOR = "11448"

protocol BeaconManagerDelegate {
    func sendCoordinates(xy: String, obj : [CLBeacon])
    func discoveredBeacon(major major: NSNumber, minor: NSNumber, proximity: CLProximity) // NOTE: #major forces first parameter to be named in function call
}

class BeaconManager: NSObject, CLLocationManagerDelegate    {
    var locationManager: CLLocationManager = CLLocationManager()
    let registeredBeaconMajor: [String] = [BEACON_BLUE_MAJOR, BEACON_GREEN_MAJOR, BEACON_PURPLE_MAJOR]
    var estimoteRegion: [CLBeaconRegion] = []
    var delegate: BeaconManagerDelegate?
    var LocationArray: NSMutableDictionary = ["ibeacon" : BEACON_PROXIMITY_UUID!]
    var xy: String?
    var userLocation: CLLocation?
    let magento = Magento.magentoSingltone
    var index = 0
    var json = [String: AnyObject]?()
    var reqJson: Dictionary<String, AnyObject>?
    var magentoResponse: JSON?

    class var sharedInstance:BeaconManager {
        return sharedBeaconManager
    }
    
    override init() {
        super.init()
        
       
        if(Reachability.isConnectedToNetwork()){
           self.magento.getBeacons({ (responseObject) in
                self.magentoResponse = responseObject
                for response in self.magentoResponse!{
                    self.estimoteRegion.append(CLBeaconRegion.init(proximityUUID:BEACON_PROXIMITY_UUID!, identifier: BEACON_REGION_NAME))
                    
                    //self.estimoteRegion.append(CLBeaconRegion.init(proximityUUID:BEACON_PROXIMITY_UUID!, major: CLBeaconMajorValue(response.1["beacon_major"].rawValue as! String)!, identifier: BEACON_REGION_NAME))
                    //i = i + 1
                    // Not workng for multiple regions
                }
                for region in self.estimoteRegion {
                    self.locationManager.startMonitoringForRegion(region)
                    self.locationManager.startRangingBeaconsInRegion(region)
                    
                }

                self.locationManager.startUpdatingLocation()
                self.locationManager.allowsBackgroundLocationUpdates = true
            })
            //
           // self.saveBeaconData()
            //self.writeBeaconTrilaterationData()
        }
        locationManager.delegate = self
    }
    
    func locationManager(manager: CLLocationManager, didStartMonitoringForRegion region: CLRegion) {
        locationManager.requestStateForRegion(region); // should locationManager be manager?
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            self.userLocation = location
            
            CLGeocoder().geocodeAddressString("Balozi smilsu 9") { (placemarks: [CLPlacemark]?, error : NSError?) in
                if let placemark = placemarks?[0] {
                    let coordinates = placemark.location!.coordinate
                    
                    
                    //let myLocation = CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude)
                    //print(myLocation)
                    
                    //print(self.userLocation?.distanceFromLocation(myLocation))
                    
                }
            }
        }
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("Failed to find user's location: \(error.localizedDescription)")
    }

    
    func locationManager(manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], inRegion region: CLBeaconRegion) {
        locationManager.requestLocation();
        var coordinates: NSDictionary?
        for response in self.magentoResponse!{
            CLGeocoder().geocodeAddressString(response.1["address"].rawString()! as String) { (placemarks: [CLPlacemark]?, error : NSError?) in
                if let placemark = placemarks?[0] {
                    let coordinates = placemark.location!.coordinate
                    let myLocation = CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude)
                    //if(Double((self.userLocation?.distanceFromLocation(myLocation))!) < 100 ){
                        //beacons.removeLast()
                        //var beaconnn: CLBeacon =
                        /*
                        for beacon in beacons {
                            if(beacon.major == response.1["beacon_major"].rawString()){
                        
                            }
                        }*/
                   // }
                    
                }
            }
        }
        for beacon in beacons {
            self.delegate?.discoveredBeacon(major: beacon.major, minor: beacon.minor, proximity: beacon.proximity)
        }
        
        if let path = NSBundle.mainBundle().pathForResource("beaconCoordinates", ofType: "plist") {
            coordinates = NSDictionary(contentsOfFile: path)
        }
        
        let beaconManager : MiBeaconTrilateration = MiBeaconTrilateration.init(witBeacons: coordinates! as [NSObject : AnyObject]);
        if let aa = beaconManager.trilaterateWithBeacons(beacons){
            do {
                xy = try NSString(data: NSJSONSerialization.dataWithJSONObject(aa, options:[]), encoding: NSUTF8StringEncoding)! as String
                let coordinates = xy!
                print(xy!)
                let coordinateArray = coordinates.characters.split{$0 == ","}.map(String.init)
                // or simply:
                // let fullNameArr = fullName.characters.split{" "}.map(String.init)
                var x = String(coordinateArray[0].characters.dropFirst())
                var y = String(coordinateArray[1].characters.dropLast())
                                let date = NSDate()
                let dataFormater = NSDateFormatter()
                dataFormater.dateFormat = "yyyy-MM-dd HH:mm:ss"
               // self.LocationArray.setObject(["date":dataFormater.stringFromDate(date), "x":x , "y":y], forKey: "\(self.index)")
                self.index += 1
                
                self.reqJson = [
                    "id": "\(self.index)",
                    "data": [
                        [
                            "date": dataFormater.stringFromDate(date),
                            "x": x,
                            "y" : y,
                            "device-uuid": UIDevice.currentDevice().identifierForVendor!.UUIDString
                            
                        ]
                    ]
                ]
                //print(self.reqJson!)
                Server.sendArray(self.reqJson!)
                
            } catch {
                
            }
        }
    }
    func locationManager(manager: CLLocationManager, didExitRegion region: CLRegion) {
        print("region exit")
        
        //Server.sendArray(self.LocationArray)
    }
    func locationManager(manager: CLLocationManager, didEnterRegion region: CLRegion) {
        for region in self.estimoteRegion {
            self.locationManager.startMonitoringForRegion(region)
            self.locationManager.startRangingBeaconsInRegion(region)

        }
    }
    
    func locationManager(manager: CLLocationManager, didDetermineState state: CLRegionState, forRegion region: CLRegion) {
        let notification = UILocalNotification()
        
        notification.fireDate = NSDate(timeIntervalSinceNow: 15)
        notification.alertBody = "CLRegionState\(state)"
        notification.alertAction = "be awesome!"
        notification.soundName = UILocalNotificationDefaultSoundName
        notification.userInfo = ["CustomField1": "w00t"]
        
       //UIApplication.sharedApplication().presentLocalNotificationNow(notification)
        
        switch state {
            case .Inside:
                print("BeaconManager:didDetermineState CLRegionState.Inside");
                //Server.init(url: "http://45.62.230.72/json.php");

            
            case .Outside:
                print("BeaconManager:didDetermineState CLRegionState.Outside");
            

            case .Unknown:
                print("BeaconManager:didDetermineState CLRegionState.Unknown");
        }
    }

    func saveBeaconData() {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true) as NSArray
        let documentsDirectory = paths.objectAtIndex(0) as! NSString
        let path = documentsDirectory.stringByAppendingPathComponent("BeaconData.plist")
        var dict: NSMutableDictionary = ["ibeacons": "beacon"]
        magento.getBeacons { (responseObject) in
            for object in responseObject! {
                dict.setObject(["id":"\(object.0)", "title": object.1["title"].rawString()!, "address" : object.1["address"].rawString()!, "uuid" : object.1["beacon_uuid"].rawString()!, "major" : object.1["beacon_major"].rawString()!, "minor" : object.1["beacon_minor"].rawString()!], forKey: "\(object.0)")

            }
            print(dict)
            dict.writeToFile(path, atomically: false)

        }
        let resultDictionary = NSMutableDictionary(contentsOfFile: path)
        print("\(resultDictionary?.description)")
    }
    
    func writeBeaconTrilaterationData() {
        let paths = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true) as NSArray
        let documentsDirectory = paths.objectAtIndex(0) as! NSString
        let path = NSBundle.mainBundle().pathForResource("beaconCoordinates", ofType: "plist")
        let resultDictionary = NSDictionary(contentsOfFile: path!)
        print("\(resultDictionary?.description)")
    }
}
let sharedBeaconManager = BeaconManager()


    