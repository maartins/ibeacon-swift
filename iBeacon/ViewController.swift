//
//  ViewController.swift
//  iBeacon
//
//  Created by Martins Saukums on 29/03/16.
//  Copyright © 2016 Martins Saukums. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if defaults.objectForKey("userLoggedIn") == nil {
            let LoginVc : LoginViewController = (self.storyboard?.instantiateViewControllerWithIdentifier("LoginViewController") as? LoginViewController)!
            self.navigationController?.presentViewController(LoginVc, animated: false, completion: nil)
        }else{
            let AppVC : AppViewController = (self.storyboard?.instantiateViewControllerWithIdentifier("AppViewController") as? AppViewController)!
            self.navigationController?.presentViewController(AppVC, animated: false, completion: nil)
        }
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

