//
//  sendToServer.swift
//  iBeacon
//
//  Created by Martins Saukums on 05/04/16.
//  Copyright © 2016 Martins Saukums. All rights reserved.
//

import Foundation
import Alamofire


class Server {
    
   static let url = "http://laravel.saukums.lv/importFromIos"
    
    static func sendArray(array: [String : AnyObject] ){
        Alamofire.request(.POST, self.url, parameters: array, encoding: .JSON)
            .responseString { response in
                // debugPrint(response)
        }
    }
    
}