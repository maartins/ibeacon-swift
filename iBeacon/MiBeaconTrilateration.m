#import "MiBeaconTrilateration.h"

@implementation MiBeaconTrilateration
{
@private
    NSDictionary* beaconCoordinates;
}


- (id)initWitBeacons:(NSDictionary *)coordinates {
    self = [super init];
    if(self) {
        beaconCoordinates = coordinates;
    }
    return self;
}

- (NSArray*)trilaterateWithBeacons:(NSArray *)beacons
{
    
    NSString *error = @"";
    NSArray *coordinates;
    NSMutableArray *useBeacons = [[NSMutableArray alloc] init];
    useBeacons = [beacons mutableCopy];
    
    NSMutableArray *beaconsToRemove;
    if (!beaconsToRemove) { beaconsToRemove = [[NSMutableArray alloc] init]; }
    
    for (CLBeacon *beacon in useBeacons) {
        if (beacon.accuracy < 0) {
            [beaconsToRemove addObject:beacon];
        }
    }

    if ([useBeacons count] > 2){
        

        CLBeacon *beacon1 = [useBeacons objectAtIndex:0];
        CLBeacon *beacon2 = [useBeacons objectAtIndex:1];
        CLBeacon *beacon3 = [useBeacons objectAtIndex:2];
        
        NSArray *beaconLocation1 = [beaconCoordinates objectForKey:[NSString stringWithFormat:@"%d", [beacon1.minor intValue]]];
        NSArray *beaconLocation2 = [beaconCoordinates objectForKey:[NSString stringWithFormat:@"%d", [beacon2.minor intValue]]];
        NSArray *beaconLocation3 = [beaconCoordinates objectForKey:[NSString stringWithFormat:@"%d", [beacon3.minor intValue]]];
        
        NSLog(@"%@",beaconLocation3);
        if (beaconLocation1 && beaconLocation2 && beaconLocation3)
        {

            NSMutableArray *ex = [[NSMutableArray alloc] initWithCapacity:0];
            double temp = 0;
            for (int i = 0; i < [beaconLocation1 count]; i++) {
                double t1 = [[beaconLocation2 objectAtIndex:i] doubleValue];
                double t2 = [[beaconLocation1 objectAtIndex:i] doubleValue];
                
                double t = t1 - t2;
                temp += (t*t);
            }
            for (int i = 0; i < [beaconLocation1 count]; i++) {
                double t1 = [[beaconLocation2 objectAtIndex:i] doubleValue];
                double t2 = [[beaconLocation1 objectAtIndex:i] doubleValue];
                double exx = (t1 - t2)/sqrt(temp);
                [ex addObject:[NSNumber numberWithDouble:exx]];
            }
            
            NSMutableArray *p3p1 = [[NSMutableArray alloc] initWithCapacity:0];
            for (int i = 0; i < [beaconLocation3 count]; i++) {
                double t1 = [[beaconLocation3 objectAtIndex:i] doubleValue];
                double t2 = [[beaconLocation1 objectAtIndex:i] doubleValue];
                double t3 = t1 - t2;
                [p3p1 addObject:[NSNumber numberWithDouble:t3]];
            }
            
            double ival = 0;
            for (int i = 0; i < [ex count]; i++) {
                double t1 = [[ex objectAtIndex:i] doubleValue];
                double t2 = [[p3p1 objectAtIndex:i] doubleValue];
                ival += (t1*t2);
            }
            
            NSMutableArray *ey = [[NSMutableArray alloc] initWithCapacity:0];
            double p3p1i = 0;
            for (int  i = 0; i < [beaconLocation3 count]; i++) {
                double t1 = [[beaconLocation3 objectAtIndex:i] doubleValue];
                double t2 = [[beaconLocation1 objectAtIndex:i] doubleValue];
                double t3 = [[ex objectAtIndex:i] doubleValue] * ival;
                double t = t1 - t2 -t3;
                p3p1i += (t*t);
            }
            for (int i = 0; i < [beaconLocation3 count]; i++) {
                double t1 = [[beaconLocation3 objectAtIndex:i] doubleValue];
                double t2 = [[beaconLocation1 objectAtIndex:i] doubleValue];
                double t3 = [[ex objectAtIndex:i] doubleValue] * ival;
                double eyy = (t1 - t2 - t3)/sqrt(p3p1i);
                [ey addObject:[NSNumber numberWithDouble:eyy]];
            }
       
            NSMutableArray *ez = [[NSMutableArray alloc] initWithCapacity:0];
            double ezx;
            double ezy;
            double ezz;
            if ([beaconLocation1 count] !=3){
                ezx = 0;
                ezy = 0;
                ezz = 0;
                
            }else{
                ezx = ([[ex objectAtIndex:1] doubleValue]*[[ey objectAtIndex:2]doubleValue]) - ([[ex objectAtIndex:2]doubleValue]*[[ey objectAtIndex:1]doubleValue]);
                ezy = ([[ex objectAtIndex:2] doubleValue]*[[ey objectAtIndex:0]doubleValue]) - ([[ex objectAtIndex:0]doubleValue]*[[ey objectAtIndex:2]doubleValue]);
                ezz = ([[ex objectAtIndex:0] doubleValue]*[[ey objectAtIndex:1]doubleValue]) - ([[ex objectAtIndex:1]doubleValue]*[[ey objectAtIndex:0]doubleValue]);
            }
            
            [ez addObject:[NSNumber numberWithDouble:ezx]];
            [ez addObject:[NSNumber numberWithDouble:ezy]];
            [ez addObject:[NSNumber numberWithDouble:ezz]];
            
            double d = sqrt(temp);
            
            double jval = 0;
            for (int i = 0; i < [ey count]; i++) {
                double t1 = [[ey objectAtIndex:i] doubleValue];
                double t2 = [[p3p1 objectAtIndex:i] doubleValue];
                jval += (t1*t2);
            }
            
            double xval = (pow(beacon1.accuracy,2) - pow(beacon2.accuracy,2) + pow(d,2))/(2*d);
            
            double yval = ((pow(beacon1.accuracy,2) - pow(beacon3.accuracy,2) + pow(ival,2) + pow(jval,2))/(2*jval)) - ((ival/jval)*xval);
            
            int zval = 0;
            
            NSMutableArray *trilateratedCoordinates = [[NSMutableArray alloc] initWithCapacity:0];
            for (int i = 0; i < [beaconLocation1 count]; i++) {
                double t1 = [[beaconLocation1 objectAtIndex:i] doubleValue];
                double t2 = [[ex objectAtIndex:i] doubleValue] * xval;
                double t3 = [[ey objectAtIndex:i] doubleValue] * yval;
                double t4 = [[ez objectAtIndex:i] doubleValue] * zval;
                double triptx = t1+t2+t3+t4;
                [trilateratedCoordinates addObject:[NSNumber numberWithDouble:triptx]];
                if (isnan(triptx))
                {
                    error = @"at least one of the calculated coordinates is NaN";
                }
            }
            coordinates = [trilateratedCoordinates copy];
        }
        else
        {
            error = @"one ore more beacons not specified in Plist";
        }
    }
    else
    {
        error = @"need at least three beacons for trilateration";
    }
    return coordinates;
}

@end
