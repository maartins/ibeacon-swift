//
//  LoginViewController.swift
//  
//
//  Created by Martins Saukums on 20/05/16.
//
//

import UIKit


class LoginViewController: UIViewController, UITextFieldDelegate, BeaconManagerDelegate,CLLocationManagerDelegate {
    var beaconManager: BeaconManager?

    let magento = Magento.magentoSingltone
    let defaults = NSUserDefaults.standardUserDefaults()
    let mySpecialNotificationKey = "lv.saukums.userLogedIn"
    
    @IBOutlet weak var customerEmail: UITextField!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var bg: UIImageView!
    @IBOutlet weak var customerPassword: UITextField!
   
    
    override func viewWillAppear(animated: Bool) {
        if((defaults.objectForKey("userLoggedIn")) != nil){
            if (defaults.objectForKey("userLoggedIn")  as! String) == "yes" {
                let AppVC : AppViewController = (self.storyboard?.instantiateViewControllerWithIdentifier("AppViewController") as? AppViewController)!
                self.navigationController?.presentViewController(AppVC, animated: false, completion: nil)
            }
        }
    }
    
    override func viewDidLoad() {
        
        var blurEffect = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        var blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        print(bg.bounds)
        blurEffectView.alpha = 0.4
        bg.addSubview(blurEffectView)
        loginView.layer.cornerRadius = 10.0
        loginView.clipsToBounds = true
        
        let imageView1 = UIImageView();
        let image1 = UIImage(named: "user");
        imageView1.image = image1;
        imageView1.frame = CGRect(x: 10, y: 5
            , width: 14, height: 16)
        customerEmail.addSubview(imageView1)
        let leftView1 = UIView.init(frame: CGRectMake(10, 0, 30, 30))
        customerEmail.leftView = leftView1;
        customerEmail.leftViewMode = UITextFieldViewMode.Always
        
        let imageView = UIImageView();
        let image = UIImage(named: "password");
        imageView.image = image;
        imageView.frame = CGRect(x: 10, y: 5
            , width: 14, height: 16)
        customerPassword.addSubview(imageView)
        let leftView = UIView.init(frame: CGRectMake(10, 0, 30, 30))
        customerPassword.leftView = leftView;
        customerPassword.leftViewMode = UITextFieldViewMode.Always
        
        super.viewDidLoad()
        
        self.beaconManager = sharedBeaconManager
        self.beaconManager!.delegate = self
        customerEmail.delegate = self
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(LoginViewController.actOnSpecialNotification), name: self.mySpecialNotificationKey, object: nil)
    }
    
    @IBAction func loginAction(sender: UIButton) {
        let array = ["username" : customerEmail.text!, "password" : customerPassword.text! ]
        magento.customerLogin(array)
        if(self.checkIfLoginSuccess()){
            print("sdfs");
        }else{
            
        }
    }
    
    func actOnSpecialNotification() {
        print("receved")
        let AppVC : AppViewController = (self.storyboard?.instantiateViewControllerWithIdentifier("AppViewController") as? AppViewController)!
        print(AppVC)
        self.navigationController?.pushViewController(AppVC, animated: false)
    }
    
    func checkIfLoginSuccess() -> Bool{
        print(defaults.objectForKey("userLoggedIn"));
        if let loginStatuss = defaults.objectForKey("userLoggedIn") {
            return (loginStatuss as! String  == "yes") ? true :false
        }else{
            return false
        }
    }
    
    func sendCoordinates(xy: String, obj: [CLBeacon]){
        for beacon: CLBeacon in obj {
            print (beacon)
            print(beaconManager!.xy);
        }
    }
    
    
    func discoveredBeacon(major major: NSNumber, minor: NSNumber, proximity: CLProximity) {
        // self.lable.text = "VC major:\(major) minor:\(minor) distance:\(proximity)";
        
    }
    
}