//
//  Magento.swift
//  iBeacon
//
//  Created by Martins Saukums on 21/05/16.
//  Copyright © 2016 Martins Saukums. All rights reserved.
//

import Foundation
import Alamofire
import SWXMLHash
import SwiftyJSON

protocol MagentoDelegate {
    func sendData(name: String, points :String)
}

class Magento {
    
    var delegate: MagentoDelegate?
    
    static let magentoSingltone = Magento()
    
    let authenticationSuccess = "Authentication complete."
    let authenticationAlreadyLogges = "You are already logged in."
    let authenticationFail  = "Invalid login or password."
    let mySpecialNotificationKey = "lv.saukums.userLogedIn"
    
    let xmlUrl = "http://magento.saukums.lv/xmlconnect/"
    let url = "http://magento.saukums.lv/"
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    func customerLogin( array:[String: String]){
        Alamofire.request(.POST, self.xmlUrl+"customer/login", parameters: array)
            .response { (request, response, data, error) in
            print(data) // if you want to check XML data in debug window.
            let xml = SWXMLHash.parse(data!)
                if(self.authenticationSuccess == xml["message"]["text"].element?.text){
                    print("login")
                    NSNotificationCenter.defaultCenter().postNotificationName(self.mySpecialNotificationKey, object: self)
                    self.defaults.setValue("yes", forKey: "userLoggedIn")
                }
            print(xml["message"]["text"].element?.text) // output the FilmID element.
        }
        print(Alamofire.au_session)
    }
    
    func customerReward() {
        Alamofire.request(.POST, self.xmlUrl+"customer/reward")
            .response { (request, response, data, error) in
                print(data) // if you want to check XML data in debug window.
                let xml = SWXMLHash.parse(data!)
                
                self.delegate?.sendData((xml["message"]["name"].element?.text)!, points: (xml["message"]["points"].element?.text)!)
        }
    }
    
    func customerLogout() {
        Alamofire.request(.POST, self.xmlUrl+"customer/logout")
            .response { (request, response, data, error) in
                let xml = SWXMLHash.parse(data!)
                print(xml)
        }

    }
    
    func getBeacons(completionHandler: (responseObject: JSON?) -> ()) {
        getBeaconsCall(completionHandler)
    }
    
    func getBeaconsCall(completionHandler: (responseObject: JSON?) -> ()) {
        Alamofire.request(.POST, self.url+"beacons/index/beacons", encoding:.JSON).validate().responseJSON { (response) in
                completionHandler(responseObject: JSON(response.result.value!))

        }

    }
    
}